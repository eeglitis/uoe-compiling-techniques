package parser;

import ast.*;
import lexer.Token;
import lexer.Tokeniser;
import lexer.Token.TokenClass;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


/**
 * @author cdubach
 */
public class Parser {

    private Token token;

    // use for backtracking (useful for distinguishing decls from procs when parsing a program for instance)
    private Queue<Token> buffer = new LinkedList<>();

    private final Tokeniser tokeniser;



    public Parser(Tokeniser tokeniser) {
        this.tokeniser = tokeniser;
    }

    public Program parse() {
        // get the first token
        nextToken();

        return parseProgram();
    }

    public int getErrorCount() {
        return error;
    }

    private int error = 0;
    private Token lastErrorToken;

    private void error(TokenClass... expected) {

        if (lastErrorToken == token) {
            // skip this error, same token causing trouble
            return;
        }

        StringBuilder sb = new StringBuilder();
        String sep = "";
        for (TokenClass e : expected) {
            sb.append(sep);
            sb.append(e);
            sep = "|";
        }
        System.out.println("Parsing error: expected ("+sb+") found ("+token+") at "+token.position);

        error++;
        lastErrorToken = token;
    }

    /*
     * Look ahead the i^th element from the stream of token.
     * i should be >= 1
     */
    private Token lookAhead(int i) {
        // ensures the buffer has the element we want to look ahead
        while (buffer.size() < i)
            buffer.add(tokeniser.nextToken());
        assert buffer.size() >= i;

        int cnt=1;
        for (Token t : buffer) {
            if (cnt == i)
                return t;
            cnt++;
        }

        assert false; // should never reach this
        return null;
    }


    /*
     * Consumes the next token from the tokeniser or the buffer if not empty.
     */
    private void nextToken() {
        if (!buffer.isEmpty())
            token = buffer.remove();
        else
            token = tokeniser.nextToken();
    }

    /*
     * If the current token is equals to the expected one, then skip it, otherwise report an error.
     * Returns the expected token or null if an error occurred.
     */
    private Token expect(TokenClass... expected) {
        for (TokenClass e : expected) {
            if (e == token.tokenClass) {
                Token cur = token;
                nextToken();
                return cur;
            }
        }

        error(expected);
        return null;
    }

    /*
    * Returns true if the current token is equals to any of the expected ones.
    */

    private boolean accept(TokenClass... expected) {
        boolean result = false;
        for (TokenClass e : expected)
            result |= (e == token.tokenClass);
        return result;
    }


    private Program parseProgram() {
        parseIncludes();
        List<VarDecl> varDecls = parseDecls();
        List<Procedure> procs = parseProcs();
        Procedure main = parseMain();
        expect(TokenClass.EOF);
        return new Program(varDecls, procs, main);
    }

    // includes are ignored, so does not need to return an AST node
    private void parseIncludes() {
	    if (accept(TokenClass.INCLUDE)) {
            nextToken();
            expect(TokenClass.STRING_LITERAL);
            parseIncludes();
        }
    }

    private List<VarDecl> parseDecls() {
    	List<VarDecl> vardecls = new LinkedList<VarDecl>();
    	// check that it is not parseProcs
    	if ((lookAhead(2).tokenClass == TokenClass.SEMICOLON)  && !(accept(TokenClass.RETURN))) {
			if (accept(TokenClass.INT) || accept(TokenClass.VOID) || accept(TokenClass.CHAR)) {
			    Type type = parseType();
			    Var var = parseVar();
			    expect(TokenClass.SEMICOLON);
			    vardecls.add(new VarDecl(type, var));
			    vardecls.addAll(parseDecls());
			    return vardecls;
			} else {
	    		// invalid type
	    		error(TokenClass.INT, TokenClass.CHAR, TokenClass.VOID);
	    		nextToken(); // move to the var token
	    		nextToken(); // move to the semicolon token
			    expect(TokenClass.SEMICOLON);
			    vardecls.addAll(parseDecls());
			    return vardecls;
			}
		} else if ((lookAhead(1).tokenClass == TokenClass.SEMICOLON) && !(accept(TokenClass.RETURN))) {
	    	// identifier missing type
	    	error(TokenClass.INT, TokenClass.CHAR, TokenClass.VOID);
	    	nextToken();
	    	expect(TokenClass.SEMICOLON);
	    	vardecls.addAll(parseDecls());
	    	return vardecls;
	    } else {
			// epsilon case
			return vardecls;
		}
    }

    private Type parseType() {
    	if (accept(TokenClass.INT)) {
    		nextToken();
    		return Type.INT;
    	} else if (accept(TokenClass.VOID)) {
    		nextToken();
    		return Type.VOID;
    	} else if (accept(TokenClass.CHAR)) {
    		nextToken();
    		return Type.CHAR;
    	} else {
    		// invalid type
    		error(TokenClass.INT, TokenClass.CHAR, TokenClass.VOID);
    		nextToken();
    		return null;
    	}
    }

    private Var parseVar() {
    	Token str = expect(TokenClass.IDENTIFIER);
    	if (str != null)
    		return new Var(str.data);
    	else
    		return null;
    }

    private List<Procedure> parseProcs() {
    	List<Procedure> procs = new LinkedList<Procedure>();
    	// check that it is neither parseDecls nor main
    	if ((accept(TokenClass.INT) || accept(TokenClass.CHAR) || accept(TokenClass.VOID))
    			&& lookAhead(1).tokenClass != TokenClass.MAIN
    			&& lookAhead(2).tokenClass == TokenClass.LPAR) {
    		Type type = parseType();
    		Token str = expect(TokenClass.IDENTIFIER);
    		String tstr;
    		if (str != null)
        		tstr = str.data;
        	else
        		tstr = null;
    		expect(TokenClass.LPAR);
    		List<VarDecl> params = parseParams();
    		expect(TokenClass.RPAR);
    		Block block = parseBody();
    		procs.add(new Procedure(type, tstr, params, block));
    		procs.addAll(parseProcs());
    		return procs;
    	} else {
    		// epsilon case
    		return procs;
    	}
    }

    private List<VarDecl> parseParams() {
    	List<VarDecl> params = new LinkedList<VarDecl>();
    	if (accept(TokenClass.INT) || accept(TokenClass.CHAR) || accept(TokenClass.VOID)) {
    		// is type and thus not empty
    		Type type = parseType();
    		Var var = parseVar();
    		params.add(new VarDecl(type, var));
    		params.addAll(parseParams2());
    		return params;
    	} else if (!accept(TokenClass.RPAR)) {
    		// no valid types but also not empty - throw error
    		error(TokenClass.RPAR);
    		nextToken();
    		return params;
    	} else
    		// epsilon case
    		return params;
    }
    
    private List<VarDecl> parseParams2() {
    	List<VarDecl> params = new LinkedList<VarDecl>();
        if (accept(TokenClass.COMMA)) {
        	nextToken();
        	Type type = parseType();
        	Var var = parseVar();
    		params.add(new VarDecl(type, var));
    		params.addAll(parseParams2());
    		return params;
        } else
        	// epsilon case
        	return params;
    }
    
    private Procedure parseMain() {
    	List<VarDecl> params = new LinkedList<VarDecl>();
    	Type type = parseMainVoid();
    	String str = parseMainMain();
    	expect(TokenClass.LPAR);
    	expect(TokenClass.RPAR);
    	Block block = parseBody();
        return new Procedure(type, str, params, block);
    }
    
    private Type parseMainVoid() {
    	Token voi = expect(TokenClass.VOID);
    	if (voi != null)
    		return Type.VOID;
    	else {
    		error(TokenClass.VOID);
    		return null;
    	}
    }
    
    private String parseMainMain() {
    	Token mai = expect(TokenClass.MAIN);
    	if (mai != null)
    		return new String("main");
    	else {
    		error(TokenClass.MAIN);
    		return null;
    	}
    }
    
    private Block parseBody() {
        expect(TokenClass.LBRA);
        List<VarDecl> varDecls = parseDecls();
        List<Stmt> stmts = parseStmtlist();
        expect(TokenClass.RBRA);
        return new Block(varDecls, stmts);
    }
    
    private List<Stmt> parseStmtlist() {
    	List<Stmt> stmts = new LinkedList<Stmt>();
        if (accept(TokenClass.LBRA) ||
        	accept(TokenClass.WHILE) ||
        	accept(TokenClass.IF) ||
        	accept(TokenClass.IDENTIFIER) ||
        	accept(TokenClass.RETURN) ||
        	accept(TokenClass.PRINT) ||
        	accept(TokenClass.READ)) {
        	stmts.add(parseStmt());
        	stmts.addAll(parseStmtlist());
        	return stmts;
        } else {
        	// epsilon case
        	return stmts;
        }
    }
	
    private Stmt parseStmt() {
    	if (accept(TokenClass.LBRA)) {
    		// Block
    		nextToken();
    		List<VarDecl> varDecls = new LinkedList<VarDecl>();
	   		if (accept(TokenClass.INT) || accept(TokenClass.CHAR) || accept(TokenClass.VOID)) {
	   			// have vardecls
	   			varDecls = parseDecls();
	   		}
	   		List<Stmt> stmts = parseStmtlist();
	   		expect(TokenClass.RBRA);
    		return new Block(varDecls, stmts);
	   	} else if (accept(TokenClass.WHILE)) {
	   		// While
	   		return parseWhile();
	   	} else if (accept(TokenClass.IF)) {
	   		// If
	   		return parseIf();
	   	} else if (accept(TokenClass.IDENTIFIER)) {
	   		if (lookAhead(1).tokenClass == TokenClass.ASSIGN) {
	   			// Assign
	   			return parseAssign();
	   		} else if (lookAhead(1).tokenClass == TokenClass.LPAR) {
	   			// FunCallStmt
	   			return parseFuncallCore();
	   		} else {
	   			error(TokenClass.ASSIGN, TokenClass.LPAR);
	   			return null;
	   		}
	   	} else if (accept(TokenClass.RETURN)) {
	   		// Return
	    	return parseReturn();
	   	} else if (accept(TokenClass.PRINT)) {
	   		if (lookAhead(2).tokenClass == TokenClass.STRING_LITERAL){
	   			// Print_s (FunCallStmt)
	   			return parsePrints();
	   		} else {
	   			// Print_c or Print_i (FunCallStmt)
	   			return parsePrintci();
	   		}
	   	} else if (accept(TokenClass.READ)) {
	   		// Read (FunCallStmt)
	   		return parseRead();
	   	} else {
	   		// invalid statement
	   		error(TokenClass.LBRA,
	   				TokenClass.WHILE,
	   				TokenClass.IF,
	   				TokenClass.IDENTIFIER,
	   				TokenClass.RETURN,
	   				TokenClass.PRINT,
	   				TokenClass.READ);
	   		return null;
	   	}
    }
    
    private While parseWhile() {
    	nextToken();
    	expect(TokenClass.LPAR);
        Expr expr = parseExp();
        expect(TokenClass.RPAR);
        Stmt stmt = parseStmt();
        return new While(expr, stmt);
    }
    
    private If parseIf() {
    	nextToken();
    	expect(TokenClass.LPAR);
        Expr expr = parseExp();
        expect(TokenClass.RPAR);
        Stmt stmt = parseStmt();
        Stmt if2 = parseIf2();
        return new If(expr, stmt, if2);
    }
    
    private Stmt parseIf2() {
        if (accept(TokenClass.ELSE)) {
        	nextToken();
        	return parseStmt();
        } else {
        	// epsilon case
        	return null;
        }
    }
    
    private Assign parseAssign() {
        Var var = parseVar();
        expect(TokenClass.ASSIGN);
        Expr expr = parseLexp();
        expect(TokenClass.SEMICOLON);
        return new Assign(var,expr);
    }
    
    private FunCallStmt parseFuncallCore() {
        FunCallExpr fce = parseFuncall();
        expect(TokenClass.SEMICOLON);
        return new FunCallStmt(fce.str, fce.exprs);
    }
    
    private FunCallExpr parseFuncall() {
    	Token str = expect(TokenClass.IDENTIFIER);
		String tstr;
		if (str != null)
    		tstr = str.data;
    	else
    		tstr = null;
        expect(TokenClass.LPAR);
        List<Expr> exprs = parseFuncall2();
        expect(TokenClass.RPAR);
        return new FunCallExpr(tstr, exprs);
    }
    
    private List<Expr> parseFuncall2() {
    	List<Expr> exprs = new LinkedList<Expr>();
        if (accept(TokenClass.IDENTIFIER)) {
        	exprs.add(parseVar());
        	exprs.addAll(parseFuncall3());
        	return exprs;
        } else
        	// epsilon case
        	return exprs;
    }
    
    private List<Expr> parseFuncall3() {
    	List<Expr> exprs = new LinkedList<Expr>();
    	if (accept(TokenClass.COMMA)) {
    		nextToken();
    		exprs.add(parseVar());
        	exprs.addAll(parseFuncall3());
        	return exprs;
        } else
        	// epsilon case
        	return exprs;
    }
    
    private Return parseReturn() {
    	nextToken();
    	if (accept(TokenClass.SEMICOLON)) {
   			nextToken();
   			return new Return(null);
   		} else if (accept(TokenClass.LPAR) ||
   				 accept(TokenClass.MINUS) ||
   				 accept(TokenClass.IDENTIFIER) ||
   				 accept(TokenClass.NUMBER) ||
   				 accept(TokenClass.CHARACTER) ||
   				 accept(TokenClass.READ)) {
   			Expr expr = parseLexp();
   			expect(TokenClass.SEMICOLON);
   			return new Return(expr);
   		} else {
   			// neither semicolon nor lexp
   			error(TokenClass.SEMICOLON,
   					TokenClass.LPAR,
   					TokenClass.MINUS,
   					TokenClass.IDENTIFIER,
   					TokenClass.NUMBER,
   					TokenClass.CHARACTER,
   					TokenClass.READ);
    		return null;
   		}
    }
    
    private FunCallStmt parsePrints() {
    	String print = expect(TokenClass.PRINT).data;
        expect(TokenClass.LPAR);
        List<Expr> expr = new LinkedList<Expr>();
        expr.add(parseStrLiteral());
        expect(TokenClass.RPAR);
        expect(TokenClass.SEMICOLON);
        return new FunCallStmt(print, expr);
    }
    
    private Expr parseStrLiteral() {
    	Token str;
    	if (accept(TokenClass.STRING_LITERAL)) {
    		str = expect(TokenClass.STRING_LITERAL);
    		return new StrLiteral(str.data);
    	} else if (accept(TokenClass.IDENTIFIER)) {
    		str = expect(TokenClass.IDENTIFIER);
    		return new StrLiteral(str.data);
    	} else {
    		// should be a problem!
    		error(TokenClass.STRING_LITERAL, TokenClass.IDENTIFIER);
    		return null;
    	}
    }
    
    private FunCallStmt parsePrintci() {
    	String print = expect(TokenClass.PRINT).data;
        expect(TokenClass.LPAR);
        List<Expr> expr = new LinkedList<Expr>();
        expr.add(parseLexp());
        expect(TokenClass.RPAR);
        expect(TokenClass.SEMICOLON);
        return new FunCallStmt(print, expr);
    }
    
    private FunCallStmt parseRead() {
    	List<Expr> expr = new LinkedList<Expr>();
    	String print = expect(TokenClass.READ).data;
        expect(TokenClass.LPAR);
        expect(TokenClass.RPAR);
        expect(TokenClass.SEMICOLON);
        return new FunCallStmt(print, expr);
    }
    
    private Expr parseExp() {
        Expr expr = parseLexp();
        if (accept(TokenClass.GT) ||
            	accept(TokenClass.LT) ||
            	accept(TokenClass.GE) ||
            	accept(TokenClass.LE) ||
            	accept(TokenClass.NE) ||
            	accept(TokenClass.EQ)) {
        	Op op = null;
	        if (accept(TokenClass.GT)) {
	        	op = Op.GT;
	        } else if (accept(TokenClass.LT)) {
	        	op = Op.LT;
	        } else if (accept(TokenClass.GE)) {
	        	op = Op.GE;
	        } else if (accept(TokenClass.LE)) {
	        	op = Op.LE;
	        } else if (accept(TokenClass.NE)) {
	        	op = Op.NE;
	        } else if (accept(TokenClass.EQ)) {
	        	op = Op.EQ;
	        } 
	        nextToken();
        	Expr expr2 = parseLexp();
        	return new BinOp(expr, op, expr2);
        } else {
        	// just lexp
        	return expr;
        }
    }
    
    private Expr parseLexp() {
        Expr expr = parseTerm();
        if (accept(TokenClass.PLUS) || accept(TokenClass.MINUS)) {
        	Op op = null;
        	if (accept(TokenClass.PLUS)) {
	        	op = Op.ADD;
	        } else if (accept(TokenClass.MINUS)) {
	        	op = Op.SUB;
	        }
        	nextToken();
        	Expr expr2 = parseLexp();
        	return new BinOp(expr, op, expr2);
        } else {
        	// just a term
        	return expr;
        }
    }
    
    private Expr parseTerm() {
        Expr expr = parseFactor();
        if (accept(TokenClass.DIV) || accept(TokenClass.TIMES)|| accept(TokenClass.MOD)) {
        	Op op = null;
        	if (accept(TokenClass.DIV)) {
	        	op = Op.DIV;
	        } else if (accept(TokenClass.TIMES)) {
	        	op = Op.MUL;
	        } else if (accept(TokenClass.MOD)) {
	        	op = Op.MOD;
	        }
        	nextToken();
        	Expr expr2 = parseTerm();
        	return new BinOp(expr, op, expr2);
        } else {
        	// just a factor
        	return expr;
        }
    }
    
    private Expr parseFactor() {
    	if (accept(TokenClass.LPAR)) {
        	nextToken();
        	Expr expr = parseLexp();
        	expect(TokenClass.RPAR);
        	return expr;
        } else if (accept(TokenClass.MINUS)) {
        	nextToken();
        	if (accept(TokenClass.IDENTIFIER)) {
        		Expr expr = parseVar();
        		return new BinOp(new IntLiteral(0), Op.SUB, expr);
        	} else if (accept(TokenClass.NUMBER)) {
        		Expr expr = parseIntLiteral();
        		return new BinOp(new IntLiteral(0), Op.SUB, expr);
        	} else {
        		// out of place minus sign
        		error(TokenClass.IDENTIFIER, TokenClass.NUMBER);
        		return null;
        	}
        } else if (accept(TokenClass.NUMBER)) {
        	return parseIntLiteral();
        } else if (accept(TokenClass.CHARACTER)) {
        	return parseChrLiteral();
        } else if (accept(TokenClass.READ)) {
        	String print = expect(TokenClass.READ).data;
            expect(TokenClass.LPAR);
            List<Expr> expr = new LinkedList<Expr>();
            expect(TokenClass.RPAR);
        	return new FunCallExpr(print, expr);
        } else if (accept(TokenClass.IDENTIFIER)) {
        	if (lookAhead(1).tokenClass == TokenClass.LPAR) {
        		// FunCallExpr
        		return parseFuncall();
        	} else {
        		// IDENT (Var)
        		return parseVar();
        	}
        } else {
        	// invalid factor
        	error(TokenClass.LPAR,
        			TokenClass.MINUS,
        			TokenClass.CHARACTER,
        			TokenClass.NUMBER,
        			TokenClass.READ,
        			TokenClass.IDENTIFIER);
        	return null;
        }
    }
    
    private Expr parseIntLiteral() {
    	Token num = expect(TokenClass.NUMBER);
        return new IntLiteral(Integer.parseInt(num.data));
    }
    
    private Expr parseChrLiteral() {
    	Token chr = expect(TokenClass.CHARACTER);
        return new ChrLiteral(chr.data.charAt(0));
    }      
}

