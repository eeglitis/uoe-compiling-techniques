package ast;

public abstract class Stmt implements Tree {
	
	public Procedure p;
    public abstract <T> T accept(ASTVisitor<T> v);
}
