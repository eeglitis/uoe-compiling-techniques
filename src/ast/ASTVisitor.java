package ast;

public interface ASTVisitor<T> {
    public T visitBlock(Block b);
    public T visitProcedure(Procedure p);
    public T visitProgram(Program p);
    public T visitVarDecl(VarDecl vd);
    public T visitVar(Var v);
    public T visitWhile(While w);
    public T visitIf(If i);
    public T visitAssign(Assign a);
    public T visitFunCallExpr(FunCallExpr fce);
    public T visitFunCallStmt(FunCallStmt fcs);
    public T visitReturn(Return r);
    public T visitStrLiteral(StrLiteral sl);
    public T visitChrLiteral(ChrLiteral cl);
    public T visitIntLiteral(IntLiteral il);
    public T visitBinOp(BinOp bo);
}
