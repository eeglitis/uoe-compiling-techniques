package ast;

public class If extends Stmt {
    public final Expr expr;
    public final Stmt stmt;
    public final Stmt if2;

    public If(Expr expr, Stmt stmt, Stmt if2) {
    	this.expr = expr;
    	this.stmt = stmt;
    	this.if2 = if2;
    }

    public <T> T accept(ASTVisitor<T> v) {
	    return v.visitIf(this);
    }
}