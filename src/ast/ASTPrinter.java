package ast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ASTPrinter implements ASTVisitor<Void> {

    private PrintWriter writer;

    public ASTPrinter(PrintWriter writer) {
            this.writer = writer;
    }

    public Void visitBlock(Block b) {
        writer.print("Block(");
        if (!b.varDecls.isEmpty()) {
        	b.varDecls.get(0).accept(this);
	        for (int i = 1; i<b.varDecls.size(); i++) {
	        	writer.print(",");
	        	b.varDecls.get(i).accept(this);
	        }
	        if (!b.stmts.isEmpty())
	        	writer.print(",");
        }
        if (!b.stmts.isEmpty()) {
        	b.stmts.get(0).accept(this);
	        for (int i = 1; i<b.stmts.size(); i++) {
	        	writer.print(",");
	        	b.stmts.get(i).accept(this);
	        }
        }
        writer.print(")");
        return null;
    }

    public Void visitProcedure(Procedure p) {
        writer.print("Procedure(");
        writer.print(p.type);
        writer.print(","+p.name+",");
        for (VarDecl vd : p.params) {
            vd.accept(this);
            writer.print(",");
        }
        p.block.accept(this);
        writer.print(")");
        return null;
    }

    public Void visitProgram(Program p) {
        writer.print("Program(");
        for (VarDecl vd : p.varDecls) {
            vd.accept(this);
            writer.print(",");
        }
        for (Procedure proc : p.procs) {
            proc.accept(this);
            writer.print(",");
        }
        p.main.accept(this);
        writer.print(")");
        writer.flush();
        return null;
    }

    public Void visitVarDecl(VarDecl vd){
        writer.print("VarDecl(");
        writer.print(vd.type+",");
        vd.var.accept(this);
        writer.print(")");
        return null;
    }

    public Void visitVar(Var v) {
        writer.print("Var(");
        writer.print(v.name);
        writer.print(")");
        return null;
    }
    
    public Void visitWhile(While w) {
        writer.print("While(");
        w.expr.accept(this);
        writer.print(",");
        w.stmt.accept(this);
        writer.print(")");
        return null;
    }

    public Void visitIf(If i) {
        writer.print("If(");
        i.expr.accept(this);
        writer.print(",");
        i.stmt.accept(this);
        if (i.if2 != null) {
        	writer.print(",");
        	i.if2.accept(this);
        }
        writer.print(")");
        return null;
    }
    
    public Void visitAssign(Assign a) {
        writer.print("Assign(");
        a.var.accept(this);
        writer.print(",");
        a.expr.accept(this);
        writer.print(")");
        return null;
    }
    
    public Void visitFunCallExpr(FunCallExpr fce) {
        writer.print("FunCallExpr(");
        writer.print(fce.str);
        for (Expr e : fce.exprs) {
        	writer.print(",");
            e.accept(this);
        }
        writer.print(")");
        return null;
    }
    
    public Void visitFunCallStmt(FunCallStmt fcs) {
        writer.print("FunCallStmt(");
        writer.print(fcs.str);
        for (Expr e : fcs.exprs) {
        	writer.print(",");
            e.accept(this);
        }
        writer.print(")");
        return null;
    }
    
    public Void visitReturn(Return r) {
        writer.print("Return(");
        if (r.expr != null) {
        	r.expr.accept(this);
        }        
        writer.print(")");
        return null;
    }
    
    public Void visitStrLiteral(StrLiteral sl) {
        writer.print("StrLiteral(");
        writer.print(sl.name);
        writer.print(")");
        return null;
    }
    
    public Void visitChrLiteral(ChrLiteral cl) {
        writer.print("ChrLiteral(");
        writer.print(cl.name);
        writer.print(")");
        return null;
    }
    
    public Void visitIntLiteral(IntLiteral il) {
        writer.print("IntLiteral(");
        writer.print(il.value);
        writer.print(")");
        return null;
    }
    
    public Void visitBinOp(BinOp bo) {
        writer.print("BinOp(");
        bo.expr.accept(this);
        writer.print(","+bo.op+",");
        bo.expr2.accept(this);
        writer.print(")");
        return null;
    }    
}
