package ast;

public class ChrLiteral extends Expr {
    public final char name;
    
    public ChrLiteral(char name){
    	this.name = name;
    }

    public <T> T accept(ASTVisitor<T> v) {
	    return v.visitChrLiteral(this);
    }
}