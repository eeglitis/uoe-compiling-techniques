package ast;

import java.util.List;

public class FunCallStmt extends Stmt {
    public final String str;
    public final List<Expr> exprs;
    public Procedure p;

    public FunCallStmt(String str, List<Expr> exprs) {
    	this.str = str;
    	this.exprs = exprs;
    }

    public <T> T accept(ASTVisitor<T> v) {
	    return v.visitFunCallStmt(this);
    }
}