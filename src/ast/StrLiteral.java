package ast;

public class StrLiteral extends Expr {
    public final String name;
    
    public StrLiteral(String name){
    	this.name = name;
    }

    public <T> T accept(ASTVisitor<T> v) {
	    return v.visitStrLiteral(this);
    }
}