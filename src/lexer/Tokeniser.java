package lexer;

import lexer.Token.TokenClass;

import java.io.EOFException;
import java.io.IOException;

/**
 * @author cdubach
 */
public class Tokeniser {

    private Scanner scanner;

    private int error = 0;
    public int getErrorCount() {
	return this.error;
    }

    public Tokeniser(Scanner scanner) {
        this.scanner = scanner;
    }

    private void error(char c, int line, int col) {
        System.out.println("Lexing error: unrecognised character ("+c+") at "+line+":"+col);
        error++;
    }
    
    private void eoferror(int line, int col) {
        System.out.println("Lexing error: unexpected end of file at "+line+":"+col);
        error++;
    }


    public Token nextToken() {
        Token result;
        try {
             result = next();
        } catch (EOFException eof) {
            // end of file, nothing to worry about, just return EOF token
            return new Token(TokenClass.EOF, scanner.getLine(), scanner.getColumn());
        } catch (IOException ioe) {
            ioe.printStackTrace();
            // something went horribly wrong, abort
            System.exit(-1);
            return null;
        }
        return result;
    }

    /*
     * To be completed
     */

    private Token next() throws IOException {

        int line = scanner.getLine();
        int column = scanner.getColumn();

	// get the next character
        char c = scanner.next();

	// temporary char for peeking
		char t = ' ';

        // skip white spaces
        if (Character.isWhitespace(c))
            return next();
    	
	//ARITHMETIC OPERATORS
		// recognises the plus operator
        if (c == '+')
	    	return new Token(TokenClass.PLUS, line, column);
		// recognises the minus operator
        if (c == '-')
	    	return new Token(TokenClass.MINUS, line, column);
		// recognises the times operator
        if (c == '*')
	    	return new Token(TokenClass.TIMES, line, column);
		// recognises the mod operator
        if (c == '%')
	    	return new Token(TokenClass.MOD, line, column);

	//DELIMITERS
		// recognises the left bracket
        if (c == '{')
	    	return new Token(TokenClass.LBRA, line, column);
		// recognises the right bracket
        if (c == '}')
	   		return new Token(TokenClass.RBRA, line, column);
		// recognises the left parenthesis
        if (c == '(')
	    	return new Token(TokenClass.LPAR, line, column);
		// recognises the right parenthesis
        if (c == ')')
	    	return new Token(TokenClass.RPAR, line, column);
		// recognises the semicolon
        if (c == ';')
	    	return new Token(TokenClass.SEMICOLON, line, column);
		// recognises the comma
        if (c == ',')
	    	return new Token(TokenClass.COMMA, line, column);

	//#INCLUDE
	if (c == '#') {
	    String inc = "include";
	    for (int i = 0; i<7; i++) {
			try {
	            c = scanner.next();
			} catch (EOFException eof) {
				eoferror(scanner.getLine(), scanner.getColumn());
		    	return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
			}
			if (c != inc.charAt(i)) {
				error(c, scanner.getLine(), scanner.getColumn());
		    	return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
			}       
	    }
	    return new Token(TokenClass.INCLUDE, scanner.getLine(), scanner.getColumn());
	}

	//COMPARISONS
	// recognizes EQ (and also ASSIGN)
	if (c == '=') {
	    // checks if it is EQ
	    try {
			c = scanner.peek();
	    } catch (EOFException eof) {
			// end of file, current token is ASSIGN
			return new Token(TokenClass.ASSIGN, line, column);
	    }
	    if (c == '=') {
	    	// found EQ, confirm counter increase
			c = scanner.next();
	        return new Token(TokenClass.EQ, scanner.getLine(), scanner.getColumn());
	    } else
	    	// can only be ASSIGN
	        return new Token(TokenClass.ASSIGN, line, column);
	}
	// recognizes LT and LE
	if (c == '<') {
	    // checks if it is LE
	    try {
			c = scanner.peek();
	    } catch (EOFException eof) {
			// end of file, current token is LT
			return new Token(TokenClass.LT, line, column);
	    }
	    if (c == '=') {
	    	// found LE, confirm counter increase
			c = scanner.next();
	        return new Token(TokenClass.LE, scanner.getLine(), scanner.getColumn());
	    } else
	    	// can only be LT
	        return new Token(TokenClass.LT, line, column);
	}
	// recognizes GT and GE
	if (c == '>') {
	    // checks if it is GE
	    try {
			c = scanner.peek();
	    } catch (EOFException eof) {
			// end of file, current token is GT
			return new Token(TokenClass.GT, line, column);
	    }
	    if (c == '=') {
	    	// found GE, confirm counter increase
			c = scanner.next();
	        return new Token(TokenClass.GE, scanner.getLine(), scanner.getColumn());
	    } else
	    	// can only be GT
	        return new Token(TokenClass.GT, line, column);
	}
	// recognizes NE
	if (c == '!') {
	    // checks for the second required symbol
	    try {
			c = scanner.peek();
	    } catch (EOFException eof) {
			// end of file, no token '!' exists
	    	eoferror(scanner.getLine(), scanner.getColumn());
			return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
	    }
	    if (c == '=') {
	    	// found NE, confirm counter increase
			c = scanner.next();
	        return new Token(TokenClass.NE, scanner.getLine(), scanner.getColumn());
	    } else
	    	// no such token exists
	    	error(c, scanner.getLine(), scanner.getColumn());
	        return new Token(TokenClass.INVALID, line, column);
	}

	//NUMBER
	if (Character.isDigit(c)) {
	    try {
			t = scanner.peek();
	    } catch (EOFException eof) {
			// end of file, return single digit number
			return new Token(TokenClass.NUMBER, Character.toString(c), line, column);
	    }
	    // not end of file
	    StringBuilder str = new StringBuilder(Character.toString(c));
	    while (Character.isDigit(c)){
			try {
		    	t = scanner.peek();
			} catch (EOFException eof) {
		    	// end of file, return current number
		    	return new Token(TokenClass.NUMBER, str.toString(), scanner.getLine(), scanner.getColumn());
			}
			if (Character.isDigit(scanner.peek())) {
		    	// next char is still a digit, confirm counter increase
	            c = scanner.next();
				str.append(Character.toString(c));
			} else
		    	// next char is not a digit, return current number
		    	return new Token(TokenClass.NUMBER, str.toString(), scanner.getLine(), scanner.getColumn());
	    }
	}

	//CHARACTER
	if (c == '\'') {
		StringBuilder str = new StringBuilder();
	    try {
			c = scanner.next();
	    } catch (EOFException eof) {
	    	eoferror(scanner.getLine(), scanner.getColumn());
			return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
	    }
	    // check for invalid symbols (tab is valid?)
	    if ((c == '\'') || (c == '\n') || (c == '\b') || (c == '\f') || (c == '\r')) {
	    	error(c, scanner.getLine(), scanner.getColumn());
			return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
	    }
	    if (c == '\"') {
	    	error(c, scanner.getLine(), scanner.getColumn());
	    	// double quote without backslash is invalid. iterate until EOF, closing single quote or new line
	    	while (!((c == '\n') || (c == '\r') || (c == '\''))) {
	    		try {
	    			c = scanner.next();
	    		} catch (EOFException eof) {
	    			eoferror(scanner.getLine(), scanner.getColumn());
	    			return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
	    		}
	    	}
	    	// found closing single quote or new line, return invalid and proceed lexing
			return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
	    }
		// check for escape characters
		if (c == '\\') {
			try {
				t = scanner.next();
	    	} catch (EOFException eof) {
	    		eoferror(scanner.getLine(), scanner.getColumn());
				return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
	    	}
			if ((t == 't') || (t == 'b') || (t == 'n') || (t == 'r') || (t == 'f') || (t == '\'') || (t == '\\') || (t == '\"')) {
				// is a valid escape character
				if ((t == '\'') || (t == '\\') || (t == '\"')) {
					str.append(Character.toString(t));
				} else {
					str.append(Character.toString(c));
					str.append(Character.toString(t));
				}
			} else {
				// not one of the 8 escape characters - invalid. iterate until EOF, closing single quote or new line
				error(t, scanner.getLine(), scanner.getColumn());
				while (!((t == '\n') || (t == '\r') || (t == '\''))) {
		    		try {
		    			t = scanner.next();
		    		} catch (EOFException eof) {
		    			eoferror(scanner.getLine(), scanner.getColumn());
		    			return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
		    		}
		    	}
		    	// found closing single quote or new line, return invalid and proceed lexing
				return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
			}
		} else {
			// not an escape character, just lex it
			str.append(Character.toString(c));
		}
	    // is a valid character, now look for closing single quote
	    try {
			c = scanner.next();
	    } catch (EOFException eof) {
	    	eoferror(scanner.getLine(), scanner.getColumn());
			return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
	    }
	    if (c == '\'') {
			// is a single character, valid
			return new Token(TokenClass.CHARACTER, str.toString(), scanner.getLine(), scanner.getColumn());
	    } else {
			// not a single character, iterate until EOF, closing single quote or new line
	    	error(c, scanner.getLine(), scanner.getColumn());
	    	while (!((c == '\n') || (c == '\r') || (c == '\''))) {
	    		try {
	    			c = scanner.next();
	    		} catch (EOFException eof) {
	    			eoferror(scanner.getLine(), scanner.getColumn());
	    			return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
	    		}
	    	}
	    	// found closing single quote or new line, return invalid and proceed lexing
	    	return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
	    }
	}

	//STRING
	if (c == '\"') {
		StringBuilder str = new StringBuilder();
	    try {
			c = scanner.next();
	    } catch (EOFException eof) {
	    	eoferror(scanner.getLine(), scanner.getColumn());
			return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
	    }
	    while (c != '\"') {
			// check for invalid symbols
			if ((c == '\n') || (c == '\r')) {
				error(c, scanner.getLine(), scanner.getColumn());
		    	return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
			}
			// check if we have escape characters
			if (c == '\\') {
		    	try {
		    		// look at next symbol with t, to prevent possible loop break
					t = scanner.next();
		    	} catch (EOFException eof) {
		    		eoferror(scanner.getLine(), scanner.getColumn());
					return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
	    	    }
		    	if ((t == 't') || (t == 'b') || (t == 'n') || (t == 'r') || (t == 'f') || (t == '\'') || (t == '\"') || (t == '\\')) {
		    		// valid escape character
		    		char escape;
		    		switch(t) {
		    		case 't': escape = '\t'; break;
		    		case 'b': escape = '\b'; break;
		    		case 'n': escape = '\n'; break;
		    		case 'r': escape = '\r'; break;
		    		case 'f': escape = '\f'; break;
		    		default: escape = t;
		    		}
		    		str.append(escape);
		    		// continue lexing
			    	try {
						c = scanner.next();
			    	} catch (EOFException eof) {
			    		eoferror(scanner.getLine(), scanner.getColumn());
			   			return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
		    	    }
		    	} else {
		    		//invalid escape character
		    		error(t, scanner.getLine(), scanner.getColumn());
		    		while (!((t == '\n') || (t == '\r') || (t == '\"'))) {
			    		try {
			    			t = scanner.next();
			    		} catch (EOFException eof) {
			    			eoferror(scanner.getLine(), scanner.getColumn());
			    			return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
			    		}
			    	}
			    	// found closing double quote or new line, return invalid and proceed lexing
					return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
		    	}
			// don't have escape characters, proceed normally
			} else {
				str.append(Character.toString(c));
		    	try {
					c = scanner.next();
		    	} catch (EOFException eof) {
		    		eoferror(scanner.getLine(), scanner.getColumn());
					return new Token(TokenClass.INVALID, scanner.getLine(), scanner.getColumn());
		    	}
	    	}
	    }
	    // found closing double quote
	    return new Token(TokenClass.STRING_LITERAL, str.toString(), scanner.getLine(), scanner.getColumn());
	}

	//MAIN
	if (c == 'm') {
		StringBuilder str = new StringBuilder(Character.toString(c));
	    String inc = "ain";
	    for (int i = 0; i<3; i++) {
	    	try {
	    		c = scanner.peek();
	    	} catch (EOFException eof) {
	    		// is identifier 
	    		return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    	}
	    	if (c != inc.charAt(i)) {
	    		// is identifier, iterate until valid symbols end
	    		while (Character.isLetterOrDigit(c) || (c == '_')) {
	    			c = scanner.next();
	    			str.append(Character.toString(c));
	    			try {
	    				c = scanner.peek();
	    			} catch (EOFException eof) {
	    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    			}
	    		}
	    		// not a valid identifier character anymore
		    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			}
	    	c = scanner.next();
	    	str.append(Character.toString(c));
	    }
	    // have read "main"
	    try {
    		c = scanner.peek();
    	} catch (EOFException eof) {
    		return new Token(TokenClass.MAIN, scanner.getLine(), scanner.getColumn());
    	}
	    // if we continue with valid identifier characters, iterate until they end
	    if (Character.isLetterOrDigit(c) || (c == '_')) {
	    	while (Character.isLetterOrDigit(c) || (c == '_')) {
    			c = scanner.next();
    			str.append(Character.toString(c));
    			try {
    				c = scanner.peek();
    			} catch (EOFException eof) {
    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
    			}
    		}
    		// not a valid identifier character anymore
	    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    // not a valid identifier character anymore - return "main"
	    } else
	    	return new Token(TokenClass.MAIN, scanner.getLine(), scanner.getColumn());
	}

	//VOID
	if (c == 'v') {
		StringBuilder str = new StringBuilder(Character.toString(c));
	    String inc = "oid";
	    for (int i = 0; i<3; i++) {
	    	try {
	    		c = scanner.peek();
	    	} catch (EOFException eof) {
	    		// is identifier 
	    		return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    	}
	    	if (c != inc.charAt(i)) {
	    		// is identifier, iterate until valid symbols end
	    		while (Character.isLetterOrDigit(c) || (c == '_')) {
	    			c = scanner.next();
	    			str.append(Character.toString(c));
	    			try {
	    				c = scanner.peek();
	    			} catch (EOFException eof) {
	    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    			}
	    		}
	    		// not a valid identifier character anymore
		    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			}
	    	c = scanner.next();
	    	str.append(Character.toString(c));
	    }
	    // have read "void"
	    try {
    		c = scanner.peek();
    	} catch (EOFException eof) {
    		return new Token(TokenClass.VOID, scanner.getLine(), scanner.getColumn());
    	}
	    // if we continue with valid identifier characters, iterate until they end
	    if (Character.isLetterOrDigit(c) || (c == '_')) {
	    	while (Character.isLetterOrDigit(c) || (c == '_')) {
    			c = scanner.next();
    			str.append(Character.toString(c));
    			try {
    				c = scanner.peek();
    			} catch (EOFException eof) {
    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
    			}
    		}
    		// not a valid identifier character anymore
	    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    // not a valid identifier character anymore - return "void"
	    } else
	    	return new Token(TokenClass.VOID, scanner.getLine(), scanner.getColumn());
	}
	
	//ELSE
	if (c == 'e') {
		StringBuilder str = new StringBuilder(Character.toString(c));
	    String inc = "lse";
	    for (int i = 0; i<3; i++) {
	    	try {
	    		c = scanner.peek();
	    	} catch (EOFException eof) {
	    		// is identifier 
	    		return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    	}
	    	if (c != inc.charAt(i)) {
	    		// is identifier, iterate until valid symbols end
	    		while (Character.isLetterOrDigit(c) || (c == '_')) {
	    			c = scanner.next();
	    			str.append(Character.toString(c));
	    			try {
	    				c = scanner.peek();
	    			} catch (EOFException eof) {
	    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    			}
	    		}
	    		// not a valid identifier character anymore
		    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			}
	    	c = scanner.next();
	    	str.append(Character.toString(c));
	    }
	    // have read "else"
	    try {
    		c = scanner.peek();
    	} catch (EOFException eof) {
    		return new Token(TokenClass.ELSE, scanner.getLine(), scanner.getColumn());
    	}
	    // if we continue with valid identifier characters, iterate until they end
	    if (Character.isLetterOrDigit(c) || (c == '_')) {
	    	while (Character.isLetterOrDigit(c) || (c == '_')) {
    			c = scanner.next();
    			str.append(Character.toString(c));
    			try {
    				c = scanner.peek();
    			} catch (EOFException eof) {
    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
    			}
    		}
    		// not a valid identifier character anymore
	    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    // not a valid identifier character anymore - return "else"
	    } else
	    	return new Token(TokenClass.ELSE, scanner.getLine(), scanner.getColumn());
	}
	
	//CHAR
	if (c == 'c') {
		StringBuilder str = new StringBuilder(Character.toString(c));
	    String inc = "har";
	    for (int i = 0; i<3; i++) {
	    	try {
	    		c = scanner.peek();
	    	} catch (EOFException eof) {
	    		// is identifier 
	    		return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    	}
	    	if (c != inc.charAt(i)) {
	    		// is identifier, iterate until valid symbols end
	    		while (Character.isLetterOrDigit(c) || (c == '_')) {
	    			c = scanner.next();
	    			str.append(Character.toString(c));
	    			try {
	    				c = scanner.peek();
	    			} catch (EOFException eof) {
	    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    			}
	    		}
	    		// not a valid identifier character anymore
		    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			}
	    	c = scanner.next();
	    	str.append(Character.toString(c));
	    }
	    // have read "char"
	    try {
    		c = scanner.peek();
    	} catch (EOFException eof) {
    		return new Token(TokenClass.CHAR, scanner.getLine(), scanner.getColumn());
    	}
	    // if we continue with valid identifier characters, iterate until they end
	    if (Character.isLetterOrDigit(c) || (c == '_')) {
	    	while (Character.isLetterOrDigit(c) || (c == '_')) {
    			c = scanner.next();
    			str.append(Character.toString(c));
    			try {
    				c = scanner.peek();
    			} catch (EOFException eof) {
    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
    			}
    		}
    		// not a valid identifier character anymore
	    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    // not a valid identifier character anymore - return "char"
	    } else
	    	return new Token(TokenClass.CHAR, scanner.getLine(), scanner.getColumn());
	}
	
	//WHILE
	if (c == 'w') {
		StringBuilder str = new StringBuilder(Character.toString(c));
	    String inc = "hile";
	    for (int i = 0; i<4; i++) {
	    	try {
	    		c = scanner.peek();
	    	} catch (EOFException eof) {
	    		// is identifier 
	    		return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    	}
	    	if (c != inc.charAt(i)) {
	    		// is identifier, iterate until valid symbols end
	    		while (Character.isLetterOrDigit(c) || (c == '_')) {
	    			c = scanner.next();
	    			str.append(Character.toString(c));
	    			try {
	    				c = scanner.peek();
	    			} catch (EOFException eof) {
	    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    			}
	    		}
	    		// not a valid identifier character anymore
		    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			}
	    	c = scanner.next();
	    	str.append(Character.toString(c));
	    }
	    // have read "while"
	    try {
    		c = scanner.peek();
    	} catch (EOFException eof) {
    		return new Token(TokenClass.WHILE, scanner.getLine(), scanner.getColumn());
    	}
	    // if we continue with valid identifier characters, iterate until they end
	    if (Character.isLetterOrDigit(c) || (c == '_')) {
	    	while (Character.isLetterOrDigit(c) || (c == '_')) {
    			c = scanner.next();
    			str.append(Character.toString(c));
    			try {
    				c = scanner.peek();
    			} catch (EOFException eof) {
    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
    			}
    		}
    		// not a valid identifier character anymore
	    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    // not a valid identifier character anymore - return "while"
	    } else
	    	return new Token(TokenClass.WHILE, scanner.getLine(), scanner.getColumn());
	}
	
	//IF and INT
	if (c == 'i') {
		StringBuilder str = new StringBuilder(Character.toString(c));
		try {
    		c = scanner.peek();
    	} catch (EOFException eof) {
    		// is identifier 
    		return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
    	}
		if (c == 'f') {
			// found "if"
			c = scanner.next();
			str.append(Character.toString(c));
			try {
		    	c = scanner.peek();
		    } catch (EOFException eof) {
		    	return new Token(TokenClass.IF, scanner.getLine(), scanner.getColumn());
		    }
			// if we continue with valid identifier characters, iterate until they end
			if (Character.isLetterOrDigit(c) || (c == '_')) {
			   	while (Character.isLetterOrDigit(c) || (c == '_')) {
		    		c = scanner.next();
		    		str.append(Character.toString(c));
		    		try {
		    			c = scanner.peek();
		    		} catch (EOFException eof) {
		    			return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
		    		}
		    	}
		    	// not a valid identifier character anymore
			   	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			// not a valid identifier character anymore - return "if"
			} else
			   	return new Token(TokenClass.IF, scanner.getLine(), scanner.getColumn());
		// not "if", but maybe "int"?
		} else if (c == 'n') {
			c = scanner.next();
			str.append(Character.toString(c));
			try {
		    	c = scanner.peek();
		    } catch (EOFException eof) {
		    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
		    }
			if (c == 't') {
				// found "int"
				c = scanner.next();
				str.append(Character.toString(c));
				try {
			    	c = scanner.peek();
			    } catch (EOFException eof) {
			    	return new Token(TokenClass.INT, scanner.getLine(), scanner.getColumn());
			    }
				// if we continue with valid identifier characters, iterate until they end
				if (Character.isLetterOrDigit(c) || (c == '_')) {
				   	while (Character.isLetterOrDigit(c) || (c == '_')) {
			    		c = scanner.next();
			    		str.append(Character.toString(c));
			    		try {
			    			c = scanner.peek();
			    		} catch (EOFException eof) {
			    			return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			    		}
			    	}
			    	// not a valid identifier character anymore
				   	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
				// not a valid identifier character anymore - return "int"
				} else
				   	return new Token(TokenClass.INT, scanner.getLine(), scanner.getColumn());
			// has "in" and something else
			} else {
			   	while (Character.isLetterOrDigit(c) || (c == '_')) {
			   		c = scanner.next();
			   		str.append(Character.toString(c));
			   		try {
			   			c = scanner.peek();
			   		} catch (EOFException eof) {
			   			return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			   		}
			   	}
			   	// not a valid identifier character anymore
			   	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			}
		// neither "if" nor "int" - "i" and something else
		}  else {
		   	while (Character.isLetterOrDigit(c) || (c == '_')) {
		   		c = scanner.next();
		   		str.append(Character.toString(c));
		   		try {
		   			c = scanner.peek();
		   		} catch (EOFException eof) {
		   			return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
		   		}
		   	}
		   	// not a valid identifier character anymore
		   	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
		}
	}
	
	//PRINT
	if (c == 'p') {
		StringBuilder str = new StringBuilder(Character.toString(c));
	    String inc = "rint_";
	    for (int i = 0; i<5; i++) {
	    	try {
	    		c = scanner.peek();
	    	} catch (EOFException eof) {
	    		// is identifier 
	    		return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    	}
	    	if (c != inc.charAt(i)) {
	    		// is identifier, iterate until valid symbols end
	    		while (Character.isLetterOrDigit(c) || (c == '_')) {
	    			c = scanner.next();
	    			str.append(Character.toString(c));
	    			try {
	    				c = scanner.peek();
	    			} catch (EOFException eof) {
	    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    			}
	    		}
	    		// not a valid identifier character anymore
		    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			}
	    	c = scanner.next();
	    	str.append(Character.toString(c));
	    }
	    // have read "print_"
	    try {
    		c = scanner.peek();
    	} catch (EOFException eof) {
    		return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
    	}
	    if ((c == 's') || (c == 'i') || (c == 'c')) {
	    	c = scanner.next();
	    	str.append(Character.toString(c));
	    	try {
	     		c = scanner.peek();
	     	} catch (EOFException eof) {
	     		return new Token(TokenClass.PRINT, str.toString(), scanner.getLine(), scanner.getColumn());
	     	}
	    	// if we continue with valid identifier characters, iterate until they end
		    if (Character.isLetterOrDigit(c) || (c == '_')) {
		    	while (Character.isLetterOrDigit(c) || (c == '_')) {
	    			c = scanner.next();
	    			str.append(Character.toString(c));
	    			try {
	    				c = scanner.peek();
	    			} catch (EOFException eof) {
	    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    			}
	    		}
	    		// not a valid identifier character anymore
		    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
		    // not a valid identifier character anymore - return "print"
		    } else
		    	return new Token(TokenClass.PRINT, str.toString(), scanner.getLine(), scanner.getColumn());
		// can only be identifier
	    } else {
	    	while (Character.isLetterOrDigit(c) || (c == '_')) {
    			c = scanner.next();
    			str.append(Character.toString(c));
    			try {
    				c = scanner.peek();
    			} catch (EOFException eof) {
    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
    			}
    		}
    		// not a valid identifier character anymore
	    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    }
	}
	
	//RETURN and READ
	if (c == 'r') {
		StringBuilder str = new StringBuilder(Character.toString(c));
		try {
    		c = scanner.peek();
    	} catch (EOFException eof) {
    		// is identifier 
    		return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
    	}
		if (c == 'e') {
			c = scanner.next();
			str.append(Character.toString(c));
			try {
	    		c = scanner.peek();
	    	} catch (EOFException eof) {
	    		// is identifier 
	    		return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    	}
			// "READ_" FORK
			if (c == 'a') {
				c = scanner.next();
				str.append(Character.toString(c));
			    String inc = "d_";
			    for (int i = 0; i<2; i++) {
			    	try {
			    		c = scanner.peek();
			    	} catch (EOFException eof) {
			    		// is identifier 
			    		return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			    	}
			    	if (c != inc.charAt(i)) {
			    		// is identifier, iterate until valid symbols end
			    		while (Character.isLetterOrDigit(c) || (c == '_')) {
			    			c = scanner.next();
			    			str.append(Character.toString(c));
			    			try {
			    				c = scanner.peek();
			    			} catch (EOFException eof) {
			    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			    			}
			    		}
			    		// not a valid identifier character anymore
				    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
					}
			    	c = scanner.next();
			    	str.append(Character.toString(c));
			    }
			    // read "read_"
			    try {
		    		c = scanner.peek();
		    	} catch (EOFException eof) {
		    		// is identifier 
		    		return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
		    	}
			    if ((c == 'i') || (c == 'c')) {
			    	c = scanner.next();
			    	str.append(Character.toString(c));
			    	try {
			     		c = scanner.peek();
			     	} catch (EOFException eof) {
			     		return new Token(TokenClass.READ, str.toString(), scanner.getLine(), scanner.getColumn());
			     	}
			    	// if we continue with valid identifier characters, iterate until they end
				    if (Character.isLetterOrDigit(c) || (c == '_')) {
				    	while (Character.isLetterOrDigit(c) || (c == '_')) {
			    			c = scanner.next();
			    			str.append(Character.toString(c));
			    			try {
			    				c = scanner.peek();
			    			} catch (EOFException eof) {
			    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			    			}
			    		}
			    		// not a valid identifier character anymore
				    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
				    // not a valid identifier character anymore - return "read"
				    } else
				    	return new Token(TokenClass.READ, str.toString(), scanner.getLine(), scanner.getColumn());
			    // can only be identifier	
			    } else {
			    	while (Character.isLetterOrDigit(c) || (c == '_')) {
		    			c = scanner.next();
		    			str.append(Character.toString(c));
		    			try {
		    				c = scanner.peek();
		    			} catch (EOFException eof) {
		    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
		    			}
		    		}
		    		// not a valid identifier character anymore
			    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			    }
			// "RETURN" FORK
			} else if (c == 't') {
				c = scanner.next();
				str.append(Character.toString(c));
			    String inc = "urn";
			    for (int i = 0; i<3; i++) {
			    	try {
			    		c = scanner.peek();
			    	} catch (EOFException eof) {
			    		// is identifier 
			    		return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			    	}
			    	if (c != inc.charAt(i)) {
			    		// is identifier, iterate until valid symbols end
			    		while (Character.isLetterOrDigit(c) || (c == '_')) {
			    			c = scanner.next();
			    			str.append(Character.toString(c));
			    			try {
			    				c = scanner.peek();
			    			} catch (EOFException eof) {
			    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			    			}
			    		}
			    		// not a valid identifier character anymore
				    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
					}
			    	c = scanner.next();
			    	str.append(Character.toString(c));
			    }
			    // read "return"
			    try {
		    		c = scanner.peek();
		    	} catch (EOFException eof) {
		    		// is identifier 
		    		return new Token(TokenClass.RETURN, scanner.getLine(), scanner.getColumn());
		    	}
			    // if we continue with valid identifier characters, iterate until they end
			    if (Character.isLetterOrDigit(c) || (c == '_')) {
			    	while (Character.isLetterOrDigit(c) || (c == '_')) {
		    			c = scanner.next();
		    			str.append(Character.toString(c));
		    			try {
		    				c = scanner.peek();
		    			} catch (EOFException eof) {
		    				return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
		    			}
		    		}
		    		// not a valid identifier character anymore
			    	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			    // not a valid identifier character anymore - return "return"
			    } else
			    	return new Token(TokenClass.RETURN, scanner.getLine(), scanner.getColumn());
			// DEFAULT FORK
			} else {
				while (Character.isLetterOrDigit(c) || (c == '_')) {
					c = scanner.next();
					str.append(Character.toString(c));
			   		try {
			   			c = scanner.peek();
			   		} catch (EOFException eof) {
			   			return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			   		}
			   	}
			   	// not a valid identifier character anymore
			   	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
			}
		// can only be identifier
		} else {
			while (Character.isLetterOrDigit(c) || (c == '_')) {
				c = scanner.next();
				str.append(Character.toString(c));
		   		try {
		   			c = scanner.peek();
		   		} catch (EOFException eof) {
		   			return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
		   		}
		   	}
		   	// not a valid identifier character anymore
		   	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
		}
	}
	
	//IDENTIFIER for all but c, e, i, m, p, r, v, w
	char[] halphabet = "_abdfghjklnoqstuxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
	for (int i = 0; i < halphabet.length; i++) {
	    if (c == halphabet[i]) {
	    	StringBuilder str = new StringBuilder(Character.toString(c));
	    	try {
	    		c = scanner.peek();
	    	} catch (EOFException eof) {
	    		// is single symbol identifier 
	    		return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    	}
	    	while (Character.isLetterOrDigit(c) || (c == '_')) {
				c = scanner.next();
				str.append(Character.toString(c));
		   		try {
		   			c = scanner.peek();
		   		} catch (EOFException eof) {
		   			return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
		   		}
		   	}
		   	// not a valid identifier character anymore
		   	return new Token(TokenClass.IDENTIFIER, str.toString(), scanner.getLine(), scanner.getColumn());
	    }
	}
	
	//COMMENTS (+ DIV)
	if (c == '/') {
	    try {
			c = scanner.peek();
	    } catch (EOFException eof) {
			// end of file, current token is DIV
			return new Token(TokenClass.DIV, line, column);
	    }
	    // SINGLE LINE COMMENT
	    if (c == '/') {
			// read until we encounter '\n' or '\r'
	    	c = scanner.next();
	        while (!((c == '\n') || (c == '\r'))) {
		    	try {
					c = scanner.next();
	   	    	} catch (EOFException eof) {
					return new Token(TokenClass.EOF, scanner.getLine(), scanner.getColumn());
	    	    }
			}
			// found '\n' or '\r'
			return next();
	    //MULTIPLE LINE COMMENT
	    } else if (c == '*') {
	    	// read until we encounter "*/"
	    	c = scanner.next();
			while (true) {
		    	try {
		        	c = scanner.next();
	   	    	} catch (EOFException eof) {
		        	return new Token(TokenClass.EOF, scanner.getLine(), scanner.getColumn());
	    	    }
		    	// repeat until '*' found
	            while (c != '*') {
		        	try {
			    		c = scanner.next();
	   	        	} catch (EOFException eof) {
			    		return new Token(TokenClass.EOF, scanner.getLine(), scanner.getColumn());
	    	        }
		    	}
		    	// found '*', check if we have '/'
		    	try {
		        	c = scanner.next();
	   	    	} catch (EOFException eof) {
		        	return new Token(TokenClass.EOF, scanner.getLine(), scanner.getColumn());
	    	    }
		    	if (c == '/')
		    		// found "*/"
		        	return next();
		    	// not "*/", continue repeating
			}
	    } else
	    	//different symbol after '/', return DIV
	    	return new Token(TokenClass.DIV, line, column);
	}

        // if we reach this point, it means we did not recognise a valid token
        error(c,line,column);
        return new Token(TokenClass.INVALID, line, column);
    }


}
