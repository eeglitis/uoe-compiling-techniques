package sem;

import ast.*;

public class NameAnalysisVisitor extends BaseSemanticVisitor<Void> {

	Scope scope = new Scope();
	
	@Override
	public Void visitBlock(Block b) {
		Scope oldScope = scope;
		scope = new Scope(scope);
		for (VarDecl vd : b.varDecls) {
            vd.accept(this);
        }
		for (Stmt st : b.stmts) {
			st.accept(this);
		}
		scope = oldScope;
		return null;
	}

	@Override
	public Void visitProcedure(Procedure p) {
		Symbol s = scope.lookupCurrent(p.name);
		if (s != null)
			error("repeated declaration for a procedure name '"+p.name+"'");
		else 
			scope.put(new ProcSymbol(p));
		Scope oldScope = scope;
		scope = new Scope(scope);
		for (VarDecl vd : p.params) {
            vd.accept(this);
        }
		for (VarDecl vd : p.block.varDecls) {
            vd.accept(this);
        }
		for (Stmt st : p.block.stmts) {
			st.accept(this);
		}
		scope = oldScope;
		return null;
	}

	@Override
	public Void visitProgram(Program p) {
		for (VarDecl vd : p.varDecls) {
            vd.accept(this);
        }
        for (Procedure proc : p.procs) {
           proc.accept(this);
        }
        p.main.accept(this);
		return null;
	}

	@Override
	public Void visitVarDecl(VarDecl vd) {
		Symbol s = scope.lookupCurrent(vd.var.name);
		if (s != null)
			error("repeated declaration for a variable name '"+vd.var.name+"'");
		else
			scope.put(new VarSymbol(vd));
		return null;
	}

	@Override
	public Void visitVar(Var v) {
		Symbol vs = scope.lookup(v.name);
		if (vs == null)
			error("variable '"+v.name+"' is not declared");
		else if (!vs.isVar())
			error("identifier '"+v.name+"' is not a variable");
		else
			v.vd = ((VarSymbol) vs).vd;
		return null;
	}

	@Override
	public Void visitWhile(While w) {
		w.expr.accept(this);
		w.stmt.accept(this);
		return null;
	}

	@Override
	public Void visitIf(If i) {
		i.expr.accept(this);
        i.stmt.accept(this);
        if (i.if2 != null) 
        	i.if2.accept(this);
		return null;
	}

	@Override
	public Void visitAssign(Assign a) {
		a.var.accept(this);
	    a.expr.accept(this);
		return null;
	}

	@Override
	public Void visitFunCallExpr(FunCallExpr fce) {
		if (!(fce.str.equals("read_i") || fce.str.equals("read_c"))) {
			//is a non-default procedure, look up
			Symbol fs = scope.lookup(fce.str);
			if (fs == null)
				error("procedure '"+fce.str+"' is not declared");
			else if (!fs.isProc())
				error("identifier '"+fce.str+"' is not a procedure");
			else
				fce.p = ((ProcSymbol) fs).p;
	        for (Expr e : fce.exprs) {
	            e.accept(this);
	        }
		}
		return null;
	}

	@Override
	public Void visitFunCallStmt(FunCallStmt fcs) {
		if (!(fcs.str.equals("print_s") || fcs.str.equals("print_i") || fcs.str.equals("print_c")
			|| fcs.str.equals("read_i") || fcs.str.equals("read_c"))) {
			//non-default procedure, look up
			Symbol fs = scope.lookup(fcs.str);
			if (fs == null)
				error("procedure '"+fcs.str+"' is not declared");
			else if (!fs.isProc())
				error("identifier '"+fcs.str+"' is not a procedure");
			else
				fcs.p = ((ProcSymbol) fs).p;
		}
        for (Expr e : fcs.exprs) {
            e.accept(this);
        }
		return null;
	}

	@Override
	public Void visitReturn(Return r) {
		if (r.expr != null)
        	r.expr.accept(this);
		return null;
	}

	@Override
	public Void visitStrLiteral(StrLiteral sl) {
		return null;
	}

	@Override
	public Void visitChrLiteral(ChrLiteral cl) {
		return null;
	}

	@Override
	public Void visitIntLiteral(IntLiteral il) {
		return null;
	}

	@Override
	public Void visitBinOp(BinOp bo) {
		bo.expr.accept(this);
        bo.expr2.accept(this);
		return null;
	}
}
