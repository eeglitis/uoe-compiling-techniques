package sem;

import ast.Procedure;

public class ProcSymbol extends Symbol {
	public Procedure p;
	
	public ProcSymbol(Procedure p) {
		this.p = p;
		this.name = p.name;
	}
}