package sem;

import ast.*;

public class TypeCheckVisitor extends BaseSemanticVisitor<Type> {
	
	@Override
	public Type visitBlock(Block b) {
		for (VarDecl vd : b.varDecls) {
            vd.accept(this);
        }
		for (Stmt st : b.stmts) {
			st.p = b.p;
			st.accept(this);
		}
		return null;
	}

	@Override
	public Type visitProcedure(Procedure p) {
		for (VarDecl vd : p.params) {
            vd.accept(this);
        }
		p.block.p = p;
		p.block.accept(this);
		return p.type;
	}

	@Override
	public Type visitProgram(Program p) {
		for (VarDecl vd : p.varDecls) {
            vd.accept(this);
        }
        for (Procedure proc : p.procs) {
           proc.accept(this);
        }
        p.main.accept(this);
		return null;
	}

	@Override
	public Type visitVarDecl(VarDecl vd) {
		if (vd.type == Type.VOID)
			error("variable '"+vd.var.name+"' cannot be of type 'void'");
		return null;
	}

	@Override
	public Type visitVar(Var v) {
		if (v.vd != null)
			v.type = v.vd.type;
		return v.type;
	}

	@Override
	public Type visitWhile(While w) {
		Type wT = w.expr.accept(this);
		if (wT != Type.INT)
			error("'while' must have a constraint of type 'int'");
		w.stmt.p = w.p;
		w.stmt.accept(this);
		return null;
	}

	@Override
	public Type visitIf(If i) {
		Type iT = i.expr.accept(this);
		if (iT != Type.INT)
			error("'if' must have a constraint of type 'int'");
		i.stmt.p = i.p;
        i.stmt.accept(this);
        if (i.if2 != null) {
        	i.if2.p = i.p;
        	i.if2.accept(this);
        }
		return null;
	}

	@Override
	public Type visitAssign(Assign a) {
		Type aT = a.var.accept(this);
	    Type aeT = a.expr.accept(this);
	    if (aT != aeT)
	    	error("incompatible types in an assign statement ('"+a.var.name+"' has "+aT+", the expression has "+aeT+")");
		return null;
	}

	@Override
	public Type visitFunCallExpr(FunCallExpr fce) {
		if (fce.str.equals("read_i")) {
			fce.type = Type.INT;
			return fce.type;
		} else if (fce.str.equals("read_c")) {
			fce.type = Type.CHAR;
			return fce.type;
		} else if (fce.p != null) {
			if (fce.exprs.size() != fce.p.params.size())
				error("incorrect number of function parameters for '"+fce.p.name+"' (given "+fce.exprs.size()+", required "+fce.p.params.size()+")");
			else {
				for (int i = 0; i<fce.exprs.size(); i++) {
					Type t = fce.p.params.get(i).type; 		// declared parameter type
					Type e = fce.exprs.get(i).accept(this);	// current parameter type
		            if (e != t)
		            	error("function parameter type mismatch (function has "+e+", procedure requires "+t+")");
		        }
				// all correct, check for return type
				if ((fce.p.type == Type.VOID) || (fce.p.type == Type.STRING))
					error("funcallexpr cannot return 'void' or 'String'");
				else {
					fce.type = fce.p.type;
					return fce.type;
				}
			}
		}
		return Type.VOID;
	}
	
	@Override
	public Type visitFunCallStmt(FunCallStmt fcs) {
		// look at print_s first
		if (fcs.str.equals("print_s")) {
			Type e = fcs.exprs.get(0).accept(this);
			if (e != Type.STRING)
				error("'print_s' must have an argument of type STRING");
		}
		// look at print_i and print_c
		else if (fcs.str.equals("print_i") || fcs.str.equals("print_c")) {
			for (int i = 0; i<fcs.exprs.size(); i++) {
				Type e = fcs.exprs.get(i).accept(this);
				if (fcs.str.equals("print_i") && (e != Type.INT))
					error("'print_i' must have arguments of type INT");
				else if (fcs.str.equals("print_c") && (e != Type.CHAR))
					error("'print_c' must have arguments of type CHAR");
			}
		}
		// look at all other non-default funcalls (discard read_i and read_c as they have no arguments)
		else if ((fcs.p != null) && !(fcs.str.equals("read_i") || fcs.str.equals("read_c"))) {
			if (fcs.exprs.size() != fcs.p.params.size())
				error("incorrect number of function parameters for '"+fcs.p.name+"' (given "+fcs.exprs.size()+", required "+fcs.p.params.size()+")");
			else {
				for (int i = 0; i<fcs.exprs.size(); i++) {
					Type t = fcs.p.params.get(i).type; 		// declared parameter type
					Type e = fcs.exprs.get(i).accept(this);	// current parameter type
		            if (e != t)
		            	error("function parameter type mismatch (function has "+e+", procedure requires "+t+")");
		        }
			}
		}
		return null;
	}

	@Override
	public Type visitReturn(Return r) {
		if (r.expr != null) {
        	Type rT = r.expr.accept(this);
        	if (rT != r.p.type)
        		error("incompatible 'return' and procedure types ('return' has '"+rT+"', procedure has '"+r.p.type+"')");
		} else {
			if (r.p.type != Type.VOID)
				error("return missing expression");
		}
		return null;
	}

	@Override
	public Type visitStrLiteral(StrLiteral sl) {
		sl.type = Type.STRING;
		return sl.type;
	}

	@Override
	public Type visitChrLiteral(ChrLiteral cl) {
		cl.type = Type.CHAR;
		return cl.type;
	}

	@Override
	public Type visitIntLiteral(IntLiteral il) {
		il.type = Type.INT;
		return il.type;
	}

	@Override
	public Type visitBinOp(BinOp bo) {
		Type b1T = bo.expr.accept(this);
        Type b2T = bo.expr2.accept(this);
        if ((bo.op == Op.ADD)
        	|| (bo.op == Op.SUB)
        	|| (bo.op == Op.MUL)
        	|| (bo.op == Op.DIV)
        	|| (bo.op == Op.MOD)) {
        	if ((b1T == Type.INT) && (b2T == Type.INT)) {
        		bo.type = Type.INT;
        		return bo.type;
        	} else
        		error("incompatible types in BinOp ("+b1T+" and "+b2T+")");
        } else if ((bo.op == Op.GT)
            	|| (bo.op == Op.LT)
            	|| (bo.op == Op.GE)
            	|| (bo.op == Op.LE)
            	|| (bo.op == Op.NE)
            	|| (bo.op == Op.EQ)) {
            if (b1T == b2T) {
            	bo.type = Type.INT;
            	return bo.type;
            } else
            	error("incompatible types in BinOp ("+b1T+" and "+b2T+")");
        }
		return Type.VOID;
	}

}
