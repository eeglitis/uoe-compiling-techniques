package sem;

import ast.VarDecl;

public class VarSymbol extends Symbol {
	public VarDecl vd;
	
	public VarSymbol(VarDecl vd) {
		this.vd = vd;
		this.name = vd.var.name;
	}
}