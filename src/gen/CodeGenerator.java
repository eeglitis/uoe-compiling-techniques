package gen;

import java.io.*;
import org.objectweb.asm.*;

import ast.ASTVisitor;
import ast.Assign;
import ast.BinOp;
import ast.Block;
import ast.ChrLiteral;
import ast.Expr;
import ast.FunCallExpr;
import ast.FunCallStmt;
import ast.If;
import ast.IntLiteral;
import ast.Procedure;
import ast.Program;
import ast.Return;
import ast.Stmt;
import ast.StrLiteral;
import ast.Type;
import ast.Var;
import ast.VarDecl;
import ast.While;

public class CodeGenerator implements ASTVisitor<Void>{
	
	private ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
	private MethodVisitor mv;

	private Scope scope = new Scope();
	
    public void emitProgram(Program p) {
    	cw.visit(Opcodes.V1_7, Opcodes.ACC_PUBLIC, "Main", null, "java/lang/Object", null);
    	p.accept(this);
    	cw.visitEnd();
        byte[] b = cw.toByteArray();
        
		//write to output stream
        FileOutputStream fos;
		try {
			fos = new FileOutputStream("out/Main.class");
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			bos.write(b);
			bos.flush();
			bos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}  
    }
    
	public Void visitProgram(Program p) {
		// visit the global variables as static fields
        for (VarDecl vd : p.varDecls) {
        	String desc = typeToDesc(vd.type);
    		cw.visitField(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, vd.var.name, desc, null, null).visitEnd();
        }
        // visit procedures
        for (Procedure proc : p.procs) {
            proc.accept(this);
        }
        // visit main
        visitMain(p.main);
		return null;
	}
	
	public Void visitProcedure(Procedure p) {	
		mv = cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, p.name, procDescriptor(p), null, null);
		visitBaseProc(p);
		return null;
	}
	
    public Void visitMain(Procedure m) {
    	mv = cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null);
    	scope.put("args", 0);
    	visitBaseProc(m);
		return null;
    }
    
    public void visitBaseProc(Procedure p) {
		mv.visitCode();
		// add parameters to scope
		for (int i = 0; i<p.params.size(); i++) {
		  	scope.put(p.params.get(i).var.name, i);
		}
		// add local variables to scope
		for (int i = 0; i<p.block.varDecls.size(); i++) {
		  	scope.put(p.block.varDecls.get(i).var.name, p.params.size()+i);
		}
		// visit statements + check for implicit return statement
		boolean hasReturn = false;
		for (Stmt s : p.block.stmts) {
			s.accept(this);
			if (s.getClass().equals(Return.class)) {
				hasReturn = true;
			}
		}
		if (!hasReturn) { // implies return type void
			mv.visitInsn(Opcodes.RETURN);
		}
		mv.visitMaxs(0, 0);
		mv.visitEnd();
    }

	public Void visitBlock(Block b) {
		Scope oldScope = scope;
		scope = new Scope(oldScope);
		// add local variables
		for (int i = 0; i<b.varDecls.size(); i++) {
        	scope.put(b.varDecls.get(i).var.name, oldScope.size()+i);
        }
		// visit statements
		for (Stmt s : b.stmts) {
			s.accept(this);
		}
		scope = oldScope;
		return null;
	}

	public Void visitVar(Var v) {		// here only used in expression calls (right side)
		if (scope.lookup(v.name) != null) { 		// local variable operation
			mv.visitVarInsn(Opcodes.ILOAD, scope.lookup(v.name));
		} else { 									// global variable operation
			String desc = typeToDesc(v.type);
			mv.visitFieldInsn(Opcodes.GETSTATIC, "Main", v.name, desc);
		}
		return null;
	}

	public Void visitWhile(While w) {
		Label l0 = new Label();
		Label l1 = new Label();
		w.expr.accept(this); // must create a single integer on the stack
		mv.visitJumpInsn(Opcodes.IFEQ, l1); // if said integer is 0 => boolean false => branch out
		mv.visitLabel(l0);
		w.stmt.accept(this);
		w.expr.accept(this); // test condition again
		mv.visitJumpInsn(Opcodes.IFNE, l0); // if said integer is not 0 => boolean true => back to loop body
		mv.visitLabel(l1);
		return null;
	}

	public Void visitIf(If i) {
		Label l0 = new Label();
		Label l1 = new Label();
		i.expr.accept(this); // must create a single integer on the stack
		mv.visitJumpInsn(Opcodes.IFEQ, l0); // if said integer is 0 => boolean false => branch out
		i.stmt.accept(this);
		mv.visitJumpInsn(Opcodes.GOTO, l1);
		mv.visitLabel(l0);
		if (i.if2 != null) {
			i.if2.accept(this);
		}
		mv.visitLabel(l1);
		return null;
	}

	public Void visitAssign(Assign a) {
		a.expr.accept(this);
		if (scope.lookup(a.var.name) != null) { 	// local variable operation
			int pos = scope.lookup(a.var.name);
			mv.visitVarInsn(Opcodes.ISTORE, pos);
		} else 	{									// global variable operation
			if (a.expr.type == Type.INT) { 	
				mv.visitFieldInsn(Opcodes.PUTSTATIC, "Main", a.var.name, "I");
			} else {		// can only be CHAR
				mv.visitFieldInsn(Opcodes.PUTSTATIC, "Main", a.var.name, "C");
			}
		}
		return null;
	}

	public Void visitReturn(Return r) {
		if (r.expr == null) {
			mv.visitInsn(Opcodes.RETURN);
		} else {  // INT or CHAR
			r.expr.accept(this);
			mv.visitInsn(Opcodes.IRETURN);
		}
		return null;
	}
	
	public Void visitFunCallStmt(FunCallStmt fcs) {
		for (Expr e : fcs.exprs) {
            e.accept(this);
        }
		if (fcs.str.equals("read_i")) {
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "IO", "read_i", "()I");
			mv.visitInsn(Opcodes.POP);
		} else if (fcs.str.equals("read_c")) {
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "IO", "read_c", "()C");
			mv.visitInsn(Opcodes.POP);
		} else if (fcs.str.equals("print_s")) {
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "IO", "print_s", "(Ljava/lang/String;)V");
		} else if (fcs.str.equals("print_i")) {
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "IO", "print_i", "(I)V");
		} else if (fcs.str.equals("print_c")) {
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "IO", "print_c", "(C)V");
		} else {
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "Main", fcs.str, procDescriptor(fcs.p));
			if (fcs.p.type != Type.VOID) {
				mv.visitInsn(Opcodes.POP);
			}
		}
		return null;
	}

	public Void visitFunCallExpr(FunCallExpr fce) {
		for (Expr e : fce.exprs) {
            e.accept(this);
        }
		if (fce.str.equals("read_i")) {
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "IO", "read_i", "()I");
		} else if (fce.str.equals("read_c")) {
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "IO", "read_c", "()C");
		} else {
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "Main", fce.str, procDescriptor(fce.p));
		}
		return null;
	}

	public Void visitChrLiteral(ChrLiteral cl) {
		mv.visitIntInsn(Opcodes.BIPUSH, (int) cl.name);
		return null;
	}

	public Void visitIntLiteral(IntLiteral il) {
		if ((il.value <= 127) && (il.value >= -128)) {
			mv.visitIntInsn(Opcodes.BIPUSH, il.value);
		} else if ((il.value <= 32767) && (il.value >= -32768)) {
			mv.visitIntInsn(Opcodes.SIPUSH, il.value);
		//} else if ((il.value <= 2147483647) && (il.value >= -2147483648)){
		} else {
			mv.visitLdcInsn(new Integer(il.value));
		}
		return null;
	}
	
	public Void visitStrLiteral(StrLiteral sl) {
		mv.visitLdcInsn(sl.name);
		return null;
	}

	public Void visitBinOp(BinOp bo) {
		bo.expr.accept(this);
		bo.expr2.accept(this);
		Label la = new Label();
		Label lb = new Label();
		switch(bo.op) {
			case ADD:
				mv.visitInsn(Opcodes.IADD);
				return null;
			case SUB:
				mv.visitInsn(Opcodes.ISUB);
				return null;
			case MUL:
				mv.visitInsn(Opcodes.IMUL);
				return null;
			case DIV:
				mv.visitInsn(Opcodes.IDIV);
				return null;
			case MOD:
				mv.visitInsn(Opcodes.IREM);
				return null;
			case GT:
				mv.visitJumpInsn(Opcodes.IF_ICMPGT, la);
				break;
			case LT:
				mv.visitJumpInsn(Opcodes.IF_ICMPLT, la);
				break;
			case GE:
				mv.visitJumpInsn(Opcodes.IF_ICMPGE, la);
				break;
			case LE:
				mv.visitJumpInsn(Opcodes.IF_ICMPLE, la);
				break;
			case NE:
				mv.visitJumpInsn(Opcodes.IF_ICMPNE, la);
				break;
			case EQ:
				mv.visitJumpInsn(Opcodes.IF_ICMPEQ, la);
				break;
		}
		mv.visitInsn(Opcodes.ICONST_0); // branch result false => int 0
		mv.visitJumpInsn(Opcodes.GOTO, lb);
		mv.visitLabel(la);
		mv.visitInsn(Opcodes.ICONST_1); // branch result true => int 1
		mv.visitLabel(lb);
		return null;
	}
	
	public String procDescriptor(Procedure p) {
		String paramdescriptors = "";
		for (int i = 0; i<p.params.size(); i++) {
			paramdescriptors = paramdescriptors + typeToDesc(p.params.get(i).type);
		}
		String procdescriptor = "("+paramdescriptors+")"+typeToDesc(p.type);
		return procdescriptor;
	}
	
	public String typeToDesc(Type t) {
		String descriptor;
		switch(t) {
			case INT:
				descriptor = "I";
				break;
			case CHAR:
				descriptor = "C";
				break;
			case VOID:
				descriptor = "V";
				break;
			default:
				descriptor = null;
				break;
		}
		return descriptor;
	}
	
	//unused, see typeToDesc instead
	public Void visitVarDecl(VarDecl vd) {
		return null;
	}

}
