package gen;

import java.util.HashMap;
import java.util.Map;

public class Scope {
	private Scope outer;
	private Map<String, Integer> localVars = new HashMap<String, Integer>();

	public Scope(Scope outer) { 
		this.outer = outer; 
	}
	
	public Scope() { this(null); }
	
	public Integer lookup(String name) {
		Scope s = new Scope(this);
		while (s != null) {
			if (s.localVars.containsKey(name))
				return s.localVars.get(name);
			s = s.outer;
		}
		return null;
	}
	
	public void put(String name, Integer i) {
		localVars.put(name, i);
	}
	
	public int size() {
		int size = 0;
		Scope s = this;
		while (s != null) {
			size = size + s.localVars.size();
			s = s.outer;
		}
		return size;
	}
}
